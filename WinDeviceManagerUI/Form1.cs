using HardwareHelperLib;
using Microsoft.Win32;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Linq;


namespace HW_Lib_Test
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern bool BlockInput([In()][MarshalAs(UnmanagedType.Bool)] bool fBlockIt);

        HH_Lib hwh = new HH_Lib();
        List<DEVICE_INFO> HardwareList;
        DEVICE_INFO _Device = new DEVICE_INFO();

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        bool isError = false;

        //Timer
        double currentsec = 0;

        public Form1()
        {
            InitializeComponent();
        }
        public void ReloadHardwareList()
        {
            HardwareList = hwh.GetAll();
            listdevices.Items.Clear();
            timer1.Start();
            foreach (var device in HardwareList)
            {
                try
                {
                    ListViewItem lvi = new ListViewItem(new string[] { device.name, device.friendlyName, device.hardwareId, device.status.ToString() });

                    if (device.name == "USB Input Device")// || device.name.Contains("touch pad"))
                    {
                        _Device.name = device.name;
                        _Device.friendlyName = device.friendlyName;
                        _Device.hardwareId = device.hardwareId;
                        //DisableDevice(_Device);
                    }
                    listdevices.Items.Add(lvi);
                }
                catch
                {
                    Close();
                }

            }
            //label1.Text = HardwareList.Count.ToString() + " Devices Attached";
        }

        void SetRegistryValue(int value)
        {
            // 1 = Disable, 0 = Enable
            const string keyName = "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System";
            const string keyName2 = "HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer";
            const string keyName3 = "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System";
            try
            {
                Registry.SetValue(keyName, "DisableLockWorkstation", value, RegistryValueKind.DWord);
                Registry.SetValue(keyName, "DisableTaskMgr", value, RegistryValueKind.DWord);
                Registry.SetValue(keyName, "DisableChangePassword", value, RegistryValueKind.DWord);
                Registry.SetValue(keyName3, "HideFastUserSwitching", value, RegistryValueKind.DWord);
                Registry.SetValue(keyName2, "NoLogoff", value, RegistryValueKind.DWord);
                Registry.SetValue(keyName3, "DisableCAD", value, RegistryValueKind.DWord);
            }
            catch
            {
                Close();
            }
        }



        private void Form1_Load(object sender, EventArgs e)
        {


            ReloadHardwareList();
            SetRegistryValue(1);
            BlockInput(true);
            Cursor.Position = new System.Drawing.Point(this.Location.X + (this.Size.Width / 2), this.Location.Y + (this.Size.Height / 2));
            Cursor.Hide();
            hwh.HookHardwareNotifications(this.Handle, true);
            Process[] processesList = Process.GetProcesses();

            Process[] processes = Process.GetProcessesByName("chrome");
            foreach (Process process in processes)
            {
                if (!process.HasExited)
                {
                    process.Kill();
                    process.WaitForExit();
                    process.Dispose();
                }
            }
            try
            {
                Process.Start("chrome.exe", ConfigurationManager.AppSettings.Get("WebLink"));
                Process bProcess = Process.GetProcessesByName("chrome").FirstOrDefault();

                if (bProcess != null)
                    SetForegroundWindow(bProcess.MainWindowHandle);
            }
            catch
            {
                isError = true;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BlockInput(false);
            EnableDevice(_Device);
            timer1.Stop();
            hwh.CutLooseHardwareNotifications(this.Handle);
            hwh = null;
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case Native.WM_DEVICECHANGE:
                    {
                        if (m.WParam.ToInt32() == Native.DBT_DEVNODES_CHANGED)
                        {
                            ReloadHardwareList();
                        }
                        break;
                    }
            }
            base.WndProc(ref m);
        }

        void EnableDevice(DEVICE_INFO Device)
        {

            hwh.CutLooseHardwareNotifications(this.Handle);
            try
            {
                hwh.SetDeviceState(Device, true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
            hwh.HookHardwareNotifications(this.Handle, true);
        }

        void DisableDevice(DEVICE_INFO Device)
        {

            hwh.CutLooseHardwareNotifications(this.Handle);

            try
            {
                hwh.SetDeviceState(Device, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            hwh.HookHardwareNotifications(this.Handle, true);
        }

        int timeLimit = int.Parse(ConfigurationManager.AppSettings.Get("timeLimit"));

        private void EnableInputs()
        {
            foreach (var device in HardwareList)
            {
                if (device.name == "USB Input Device")// || device.name.Contains("touch pad"))
                {
                    _Device.name = device.name;
                    _Device.name = device.friendlyName;
                    _Device.name = device.hardwareId;
                    _Device.name = device.status.ToString();
                    EnableDevice(_Device);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!isError)
            {
                BlockInput(true);

                Process[] chromeInstances = Process.GetProcessesByName("chrome");
                if (chromeInstances.Length > 0)
                {
                    if (currentsec == 0)
                    {
                        currentsec = DateTime.Now.TimeOfDay.TotalSeconds;
                    }
                    if (currentsec + timeLimit < DateTime.Now.TimeOfDay.TotalSeconds)
                    {
                        SetRegistryValue(0);
                        BlockInput(false);
                        EnableInputs();
                        timer1.Stop();
                        this.Dispose();
                        Close();
                    }
                }
            }
            else
            {
                EnableInputs();
                SetRegistryValue(0);
                BlockInput(false);
                timer1.Stop();
                MessageBox.Show("Please Install Chrome.", "Chrome Not Found ! ");
                this.Dispose();
                Close();
            }
           
        }

        private void enableBtn_Click(object sender, EventArgs e)
        {
            //Enable
            if (listdevices.SelectedIndices.Count == 0)
                return;

            hwh.CutLooseHardwareNotifications(this.Handle);
            try
            {
                hwh.SetDeviceState(HardwareList[listdevices.SelectedIndices[0]], true);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }
            hwh.HookHardwareNotifications(this.Handle, true);
        }


        private void disableBtn_Click(object sender, EventArgs e)
        {
            //Disable
            if (listdevices.SelectedIndices.Count == 0)
                return;

            hwh.CutLooseHardwareNotifications(this.Handle);

            try
            {
                hwh.SetDeviceState(HardwareList[listdevices.SelectedIndices[0]], false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK);
            }

            hwh.HookHardwareNotifications(this.Handle, true);
        }
    }
}